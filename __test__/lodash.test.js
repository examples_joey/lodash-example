const _ = require('lodash')

test(`
_.defaults({ a: 1 }, { a: 3, b: 2 })
將兩個物件融合，重複的項目以前者優先
`, () => {
    const obj = _.defaults({ a: 1 }, { a: 3, b: 2 })
    expect(obj).toEqual({ a: 1, b: 2 })
})

test(`
_.partition([1, 2, 3, 4], n => n > 3)
將結果進行分類，相同結果的放置一組
`, () => {
    const arr = _.partition([1, 2, 3, 4], n => n > 3)
    expect(arr).toEqual([[4], [1, 2, 3]])
})

test(`
_.chunk([1, 2, 3, 4, 5], 3)
將陣列按照固定數量切割
`, () => {
    const arr = _.chunk([1, 2, 3, 4, 5], 3)
    expect(arr).toEqual([[1, 2, 3], [4, 5]])
})